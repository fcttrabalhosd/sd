package microgram.impl.clt;

import java.net.URI;

import microgram.impl.clt.rest.RestProfilesClient;
import microgram.impl.clt.soap.SoapProfilesClient;
import microgram.api.java.Profiles;

public class ProfileClientFactory {

	private static final String REST = "/rest";
	private static final String SOAP = "/soap";

	public static Profiles getProfileClient(URI uri) {
		String uriString = uri.toString();
		if (uriString.endsWith(REST))
			return new RestProfilesClient(uri);
		else if (uriString.endsWith(SOAP))
			return new SoapProfilesClient(uri);

		throw new RuntimeException("Unknown service type..." + uri);
	}
}

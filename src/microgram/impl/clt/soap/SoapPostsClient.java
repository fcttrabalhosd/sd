package microgram.impl.clt.soap;

import java.net.URI;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import discovery.Discovery;
import microgram.api.Post;
import microgram.api.java.Posts;
import microgram.api.java.Result;
import microgram.api.soap.SoapPosts;
import microgram.api.soap.SoapProfiles;
import microgram.impl.srv.soap.PostsSoapServer;

public class SoapPostsClient extends SoapClient implements Posts {

	SoapPosts impl;
	static final QName QNAME = new QName(SoapPosts.NAMESPACE, SoapPosts.NAME);

	public SoapPostsClient() throws Exception {
		this( Discovery.findUrisOf(PostsSoapServer.SERVICE, 1)[0]); //TODO
	}
	
	
	public SoapPostsClient(URI serverUri) {
		super(serverUri, QNAME);
	}
	
	@Override
	public Result<Post> getPost(String postId) {
		return super.tryCatchResult(() -> impl().getPost(postId));
	}
	
	
	private SoapPosts impl() {
		if( impl == null ) {
			this.impl = service.getPort(microgram.api.soap.SoapPosts.class);
		}
		return impl;
	}


	@Override
	public Result<String> createPost(Post post) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Result<Void> deletePost(String postId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Result<Void> like(String postId, String userId, boolean isLiked) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Result<Boolean> isLiked(String postId, String userId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Result<List<String>> getPosts(String userId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Result<List<String>> getFeed(String userId) {
		// TODO Auto-generated method stub
		return null;
	}
}

package microgram.impl.clt.soap;

import java.net.URI;
import java.util.List;

import javax.xml.namespace.QName;

import discovery.Discovery;
import microgram.api.Profile;
import microgram.api.java.Profiles;
import microgram.api.java.Result;
import microgram.api.soap.SoapPosts;
import microgram.api.soap.SoapProfiles;
import microgram.impl.srv.soap.PostsSoapServer;
import microgram.impl.srv.soap.ProfilesSoapServer;

public class SoapProfilesClient extends SoapClient implements Profiles {

	SoapProfiles impl;
	static final QName QNAME = new QName(SoapPosts.NAMESPACE, SoapPosts.NAME);
	
	public SoapProfilesClient() throws Exception {
		this( Discovery.findUrisOf(ProfilesSoapServer.SERVICE, 1)[0]);
	}
	public SoapProfilesClient(URI serverUri) {
		super(serverUri, QNAME);
	}
	
	private SoapProfiles impl() {
		if( impl == null ) {
			this.impl = service.getPort(microgram.api.soap.SoapProfiles.class);
		}
		return impl;
	}

	@Override
	public Result<Profile> getProfile(String userId) {
		return null;
	}

	@Override
	public Result<Void> createProfile(Profile profile) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Result<Void> deleteProfile(String userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Result<List<Profile>> search(String prefix) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Result<Void> follow(String userId1, String userId2, boolean isFollowing) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Result<Boolean> isFollowing(String userId1, String userId2) {
		return super.tryCatchResult(() -> impl().isFollowing(userId1, userId2));
	}

}

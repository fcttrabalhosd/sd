package microgram.impl.srv.java;

import static microgram.api.java.Result.error;
import static microgram.api.java.Result.ok;
import static microgram.api.java.Result.ErrorCode.CONFLICT;
import static microgram.api.java.Result.ErrorCode.NOT_FOUND;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import kakfa.KafkaPublisher;
import kakfa.KafkaUtils;
import microgram.api.Post;
import microgram.api.Profile;
import microgram.api.java.Result;
import microgram.api.java.Result.ErrorCode;
import microgram.impl.srv.java.JavaPosts.PostsEventKeys;
import microgram.impl.srv.rest.ProfilesRestServer;
import microgram.impl.srv.rest.RestResource;

import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import kakfa.KafkaSubscriber;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.client.ClientConfig;

public class JavaProfiles extends RestResource implements microgram.api.java.Profiles {

	protected Map<String, Profile> users = new ConcurrentHashMap<String, Profile>();
	protected Map<String, Set<String>> followers = new ConcurrentHashMap<String,  Set<String>>();
	protected Map<String, Set<String>> following = new ConcurrentHashMap<String,  Set<String>>();
	private int nPost=0;
	
	public static final String PROFILES_EVENTS = "Microgram-ProfilesEvents";
	
	
	enum ProfilesEventKeys{
		DELETE
	}
	
	final KafkaPublisher kafka;
	
	public JavaProfiles() {
		kafka = new KafkaPublisher();
		KafkaUtils.createTopics(Arrays.asList(JavaProfiles.PROFILES_EVENTS));
	}
	@Override
	public Result<Profile> getProfile(String userId) {
		Profile res = users.get( userId );
		if( res == null ) 
			return error(NOT_FOUND);

		res.setFollowers( followers.get(userId).size() );
		res.setFollowing( following.get(userId).size() );

		List<String> topics = Arrays.asList(JavaPosts.POSTS_EVENTS);
		KafkaSubscriber subscriber = new KafkaSubscriber(topics);
		
		nPost=0;
		subscriber.consume((topic, key, value) -> {
			if(key.equals(PostsEventKeys.CREATE.name()))
				nPost++;
			else if(key.equals(PostsEventKeys.DELETE.name()))
				nPost--;

		});
		res.setPosts(nPost);
		return ok(res);
	}

	@Override
	public Result<Void> createProfile(Profile profile) {
		Profile res = users.putIfAbsent( profile.getUserId(), profile );
		if( res != null ) 
			return error(CONFLICT);
		
		followers.put( profile.getUserId(), new HashSet<>());
		following.put( profile.getUserId(), new HashSet<>());
		
		return ok();
	}
	
	@Override
	public Result<Void> deleteProfile(String userId) {
		Profile res1 = users.remove(userId);
		if(res1 != null){
			followers.remove(userId);
			following.remove(userId);
			
			followers.forEach((key, value) -> {
				   if(value.contains(userId)) {
					   value.remove(userId);
					   followers.replace(key, value);
				   }
				   
				});
			following.forEach((key, value) -> {
				   if(value.contains(userId)) {
					   value.remove(userId);
					   followers.replace(key, value);
				   }
				   
				});
			kafka.publish(PROFILES_EVENTS, ProfilesEventKeys.DELETE.name(), userId);
			return ok();	
		}
		else
			return error(NOT_FOUND);
	}
	
	@Override
	public Result<List<Profile>> search(String prefix) {
		return ok(users.values().stream()
				.filter( p -> p.getUserId().startsWith( prefix ) )
				.collect( Collectors.toList()));
	}

	@Override
	public Result<Void> follow(String userId1, String userId2, boolean isFollowing) {		
		Set<String> s1 = following.get( userId1 );
		Set<String> s2 = followers.get( userId2 );
		
		if( s1 == null || s2 == null)
			return error(NOT_FOUND);
		
		if( isFollowing ) {
			boolean added1 = s1.add(userId2 ), added2 = s2.add( userId1 );
			if( ! added1 || ! added2 )
				return error(CONFLICT);		
		} else {
			boolean removed1 = s1.remove(userId2), removed2 = s2.remove( userId1);
			if( ! removed1 || ! removed2 )
				return error(NOT_FOUND);					
		}
		return ok();
	}

	@Override
	public Result<Boolean> isFollowing(String userId1, String userId2) {

		Set<String> s1 = following.get( userId1 );
		Set<String> s2 = followers.get( userId2 );
		
		if( s1 == null || s2 == null)
			return error(NOT_FOUND);
		else
			return ok(s1.contains( userId2 ) && s2.contains( userId1 ));
	}
}

package microgram.impl.srv.java;

import static microgram.api.java.Result.error;
import static microgram.api.java.Result.ok;
import static microgram.api.java.Result.ErrorCode.CONFLICT;
import static microgram.api.java.Result.ErrorCode.INTERNAL_ERROR;
import static microgram.api.java.Result.ErrorCode.NOT_FOUND;
import static microgram.api.java.Result.ErrorCode.NOT_IMPLEMENTED;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import microgram.api.Post;
import microgram.api.java.Posts;
import microgram.api.java.Profiles;
import microgram.api.java.Result;
import microgram.api.java.Result.ErrorCode;
import microgram.api.rest.RestProfiles;
import microgram.impl.srv.java.JavaPosts.PostsEventKeys;
import microgram.impl.srv.java.JavaProfiles.ProfilesEventKeys;
import microgram.impl.srv.rest.ProfilesRestServer;
import microgram.impl.srv.rest.RestMediaResources;
import utils.Hash;

import java.util.concurrent.ConcurrentHashMap;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.client.ClientConfig;

import discovery.Discovery;
import microgram.impl.clt.ProfileClientFactory;
import kakfa.KafkaPublisher;
import kakfa.KafkaSubscriber;
import kakfa.KafkaUtils;

import kakfa.KafkaUtils;
import microgram.api.java.Media;
import microgram.api.java.MediaStorage;
import microgram.api.java.Result;
import utils.Hash;

import microgram.impl.clt.rest.*;

public class JavaPosts implements Posts {

	protected Map<String, Post> posts = new ConcurrentHashMap<String, Post>();
	protected Map<String, Set<String>> likes = new ConcurrentHashMap<String,  Set<String>>();
	protected Map<String, Set<String>> userPosts = new ConcurrentHashMap<String,  Set<String>>();
	
	public static final String POSTS_EVENTS = "Microgram-PostsEvents";
	
	enum PostsEventKeys {
		CREATE,DELETE
	};
	
	final KafkaPublisher kafka;
	
	public JavaPosts() {	
		kafka = new KafkaPublisher();
		KafkaUtils.createTopics(Arrays.asList(JavaPosts.POSTS_EVENTS));
	}
	
	@Override
	public Result<Post> getPost(String postId) {
		List<String> topics = Arrays.asList(JavaProfiles.PROFILES_EVENTS);
		KafkaSubscriber subscriber = new KafkaSubscriber(topics);
		subscriber.consume((topic,key,value) -> {
			if(key.equals(ProfilesEventKeys.DELETE.name()))
				profileDeleted(value);
		});
		
		Post res = posts.get(postId);	
		if (res != null) {
			return ok(res);
		}
		else
			return error(NOT_FOUND);
	}

	@Override
	public  Result<Void> deletePost(String postId) {
		Post res = posts.remove(postId);
		if (res != null) {
			Set<String> pos = userPosts.get(res.getOwnerId());
			pos.remove(postId);
			kafka.publish(POSTS_EVENTS, PostsEventKeys.DELETE.name(), res.getOwnerId());
			return ok();
		}else
			return error(NOT_FOUND);
	}

	@Override
	public Result<String> createPost(Post post) {
		String postId = Hash.of(post.getOwnerId(), post.getMediaUrl());
		if (posts.putIfAbsent(postId, post) == null) {

			likes.put(postId, new HashSet<>());

			Set<String> posts = userPosts.get(post.getOwnerId());
			if (posts == null)
				userPosts.put(post.getOwnerId(), posts = new LinkedHashSet<>());

			posts.add(postId);
		}
		return ok(postId);
	}

	@Override
	public  Result<Void> like(String postId, String userId, boolean isLiked) {
		
		Set<String> res = likes.get(postId);
		if (res == null)
			return error( NOT_FOUND );

		if (isLiked) {
			if (!res.add(userId))
				return error( CONFLICT );
		} else {
			if (!res.remove(userId))
				return error( NOT_FOUND );
		}

		getPost(postId).value().setLikes(res.size());
		return ok();
	}

	@Override
	public Result<Boolean> isLiked(String postId, String userId) {
		Set<String> res = likes.get(postId);
		
		if (res != null)
			return ok(res.contains(userId));
		else
			return error( NOT_FOUND );
	}

	@Override
	public  Result<List<String>> getPosts(String userId) {
		Set<String> res = userPosts.get(userId);
		if (res != null)
			return ok(new ArrayList<>(res));
		else
			return error( NOT_FOUND );
	}
	
	
	@Override
	public Result<List<String>> getFeed(String userId) {
		
		List<String> feed = new ArrayList<>();
		URI[] instances;
		try {
			instances = Discovery.findUrisOf(ProfilesRestServer.SERVICE, 1);
		} catch (Exception e) {
			return Result.error(INTERNAL_ERROR);
		}
		if( instances.length > 0 ) {
			Profiles profile = ProfileClientFactory.getProfileClient(instances[0]);
			
			userPosts.forEach((key, value) -> {
				Result<Boolean> r = profile.isFollowing(userId, key);
				
				if (r.isOK() && r.value()==true) 
					feed.addAll(userPosts.get(key));	
				//falta verificar quando nao consegue chamar o outro serviço
			});
			
			
		}else
			return Result.error(INTERNAL_ERROR);

		return ok(feed);
		
		
//		ClientConfig config = new ClientConfig();
//		Client client = ClientBuilder.newClient(config);
//		
//		WebTarget target = client.target( serverUrl ).path( RestProfiles.PATH );
//		
//		userPosts.forEach((key, value) -> {
//			Response r = target.path("/"+userId+"/following/"+key)
//					.request()
//					.accept(MediaType.APPLICATION_JSON)
//					.get();
//			
//			if( r.getStatus() == Status.OK.getStatusCode() && r.hasEntity() &&  r.readEntity(Boolean.class )) {
//				feed.addAll(userPosts.get(key));
//			}
//		});
//		return ok(feed);
	}
	
	private void profileDeleted(String userId) {

		//apagar todos os Posts e likes do userId
				
		
	}
}

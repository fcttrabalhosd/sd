package microgram.impl.srv.soap;

import java.net.InetSocketAddress;
import java.util.logging.Logger;

import javax.xml.ws.Endpoint;

import com.sun.net.httpserver.HttpServer;

import discovery.Discovery;
import microgram.impl.srv.soap.*;
import microgram.api.soap.SoapProfiles;
import utils.IP;


public class ProfilesSoapServer {
	private static Logger Log = Logger.getLogger(ProfilesSoapServer.class.getName());

	static {
		System.setProperty("java.net.preferIPv4Stack", "true");
		System.setProperty("java.util.logging.SimpleFormatter.format", "%4$s: %5$s");
	}
	
	public static final int PORT = 7777;
	public static final String SERVICE = "Microgram-Profiles";
	public static String SERVER_BASE_URI = "http://%s:%s/soap";
	public static String SOAP_BASE_PATH = "/soap/" + SoapProfiles.NAME;
	
	public static void main(String[] args) throws Exception {
		// Create an HTTP server, accepting requests at PORT (from all local interfaces)
		HttpServer server = HttpServer.create(new InetSocketAddress("0.0.0.0", PORT), 0);

		// Create the SOAP Endpoint
		Endpoint soapEndpoint = Endpoint.create(new ProfilesWebService());

		// Publish the SOAP webservice, under the "http://<ip>:<port>/soap"
		soapEndpoint.publish(server.createContext(SOAP_BASE_PATH));

		// Start Serving Requests: both SOAP Requests
		server.start();
		
		String ip = IP.hostAddress();
		
		String serverURI = String.format(SERVER_BASE_URI, ip, PORT);
		Discovery.announce(SERVICE, serverURI);
		
		Log.info(String.format("%s Soap Server ready @ %s\n", SERVICE, ip + ":" + PORT));

	}
}
